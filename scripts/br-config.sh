#!/bin/bash

# Script to set up build directories
# Need to be called with 1 argument: path to our custom buildroot config file

set -xe

TOPDIR=$(realpath $(dirname $0)/../../)

if [ ! -r "$1" ]; then
	echo "Config file not specified!"
	exit 1
fi

BUILDROOT=$TOPDIR/buildroot
CONFIG=$(realpath $1)
BUILD_DIR=$TOPDIR/br_build/$(basename -- $1)

mkdir -p $BUILD_DIR

cd $BUILDROOT
time make defconfig BR2_DEFCONFIG=$CONFIG O=$BUILD_DIR

