#!/bin/bash

# Script to start build
# will update the default script with the one from latest build

# Need 1 argument: path to the out-of-tree build directory

set -xe

TOPDIR=$(realpath $(dirname $0)/../../)
export BR2_DL_DIR=$TOPDIR/br_dl
export BR2_CCACHE_DIR=$TOPDIR/br_ccache

BUILD_DIR=$1
if [ ! -d "$BUILD_DIR" ]; then
	echo "Build directory < $BUILD_DIR > invalid!"
	exit 1
fi


# update original config
CFGDIR=$(realpath $(dirname $0)/../configs)
ORIG_CONFIG_FILE="$CFGDIR/$(basename $BUILD_DIR)"

if [ ! -w "$ORIG_CONFIG_FILE" ]; then
	echo "Original config file not found: $ORIG_CONFIG_FILE"
	exit 1
else
	cp "$BUILD_DIR/.config" "$ORIG_CONFIG_FILE"
fi

mkdir -p $BR2_DL_DIR
mkdir -p $BR2_CCACHE_DIR

# build!
cd $BUILD_DIR
time make
